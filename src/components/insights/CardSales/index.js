import React, { Component } from 'react';
import '../CardContainer/Cards.css'
import './styles.css'

class CardSales extends Component {
  componentDidMount() {
    fetch('https://my-json-server.typicode.com/DiegoRamires/json-api/db/')
  }
  render() {
    return (
      <div className="row">
        <div className="col s12 m5">
          <div className="card-panel bg-blue card-square">
            <span className="white-text">
              <i className="fa fa-dollar visitor-eye" />
              670 Sales
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default CardSales