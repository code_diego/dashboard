import React, { Component } from 'react';
import '../CardContainer/Cards.css'
import './styles.css'

class CardUser extends Component {
  render() {
    return (
      <div className="row">
        <div className="col s12 m5">
          <div className="card-panel bg-yellow card-square">
            <span className="white-text">
              <i className="fa fa-users visitor-eye" />
              1230 Users
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default CardUser