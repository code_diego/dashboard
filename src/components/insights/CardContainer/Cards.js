import React, { Component } from 'react'
import Visitor from '../CardVisitor/index'
import User from '../CardUser/index'
import Sales from '../CardSales/index'
import Order from '../CardOrder'

class Cards extends Component {
  render() {
    return (
      <div className="row">
        <div className="card-container">
          <Visitor />
          <User />
          <Sales />
          <Order />
        </div>
      </div>
    );
  }
}

export default Cards;