import React, { Component } from 'react';
import '../CardContainer/Cards.css'
import './styles.css'

class CardOrder extends Component {
  render() {
    return (
      <div className="row">
        <div className="col s12 m3">
          <div className="card-panel bg-violet card-square">
            <span className="white-text">
              <i className="fa fa-shopping-cart visitor-eye" />
              400 Orders
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default CardOrder