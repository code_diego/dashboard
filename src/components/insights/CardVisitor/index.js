import React, { Component } from 'react';
import '../CardContainer/Cards.css'
import './styles.css'

class CardVisitor extends Component {
  render() {
    return (
      <div className="row">
        <div className="col s12 m5">
          <div className="card-panel bg-green card-square">
            <span className="white-text">
              <i className="fa fa-eye visitor-eye" />
              122500 Visitors
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default CardVisitor