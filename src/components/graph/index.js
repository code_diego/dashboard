import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2'

import "./index.css"

class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      chartData: props.chartData
    }
  }

  static defaultProps = {
    displayTitle: true,
    displayLegend: false,
  }

  render() {
    return (
      <div className="chart">
        <Bar
          data={this.state.chartData}
          options={{
            title: {
              display: this.props.displayTitle,
              text: "Site Traffic Overview",
              fontSize: 16,
              position: "top"
            },
            legend: {
              display: this.props.displayLegend
            }
          }}
        />
      </div>
    );
  }
}

export default index;