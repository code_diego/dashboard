import React, { Component } from 'react'
import serializeForm from 'form-serialize'

import './style.css'
class Chat extends Component {
  handleSubmit = (e) => {
    e.preventDefault()
    const values = serializeForm(e.target, { hash: true })
    console.log(values)
  }
  render() {
    return (
      <div className="row">
        <div className="col m12 card-white card-chat">
          <h6 className="header text-bold">
            Reports
          </h6>
          <div className="divider"></div>
          <div className="card-image">
            <img src=""></img>
          </div>
          <div className="card-stacked">
            <div className="card-content">
              <div className="header fs-16 bold chat-user">Joe Montana</div>
              <div className="card-content fs-16">
                Cronut chambray flannel, seitan adaptogen cloud bread raw denim
              </div>
              <span className="time">1 hour ago</span>
            </div>
          </div>
          <div className="card-image">
            <img src=""></img>
          </div>
          <div className="card-stacked">
            <div className="card-content">
              <div className="header fs-16 bold chat-user">Marie Bell</div>
              <div className="card-content fs-16">
                Mustache mumblecore marfa waistcoat chillwave leggings copper mug.
              </div>
              <span className="time">1 hour ago</span>
            </div>
          </div>
          <div>
            <form onSubmit={this.handleSubmit}>
              <input type="text" name="conversation" />
              <button className="direction-right">Send</button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

export default Chat