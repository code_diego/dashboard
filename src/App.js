import React, { Component } from 'react'

import './styles/App.css'
import Header from './components/header'
import Cards from './components/insights/CardContainer/Cards'
import Graph from './components/graph'
import Chat from './components/chat'

class App extends Component {
  constructor() {
    super()
    this.state = {
      chartData: {},
      talks: []
    }
  }

  componentWillMount() {
    this.getChartData()
  }

  getChartData() {
    // Api call here
    this.setState({
      chartData: {
        labels: [
          "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"
        ],
        datasets: [
          {
            label: "Sales",
            data: [856, 999, 645, 834, 989, 790, 1010],
            backgroundColor: 'rgb(76,132,255,0.8)'
          }
        ]
      }
    })
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col m12">
              <Header />
              <Cards />
            </div>
            <div className="col m8 s12">
              <Graph chartData={this.state.chartData} />
            </div>
            <div className="col m4 s12">
              <Chat />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App
