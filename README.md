This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instructions to run on development mode

Install the dependencies and run server:<br>
```
  npm install
  npm start

```